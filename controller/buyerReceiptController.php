<?php 
class ByerReceiptController{

    private $conectar;
    private $Connection;

    public function __construct() {
        if(!isset($_SESSION["userid"]) || trim($_SESSION["userid"])=='' ){
            header('Location: /mvc/login');

        }

        require_once  __DIR__ . "/../core/Conectar.php";
        require_once  __DIR__ . "/../model/buyer_receipt.php";
        require_once  __DIR__ . "/../model/product.php";

        $this->conectar=new Conectar();
        $this->Connection=$this->conectar->Connection();
        
    }


    public function run($action){
        switch($action)
        { 
            case "index" :
                $this->index();
                break;
           case "index_entry_by" :
                $this->index_entry_by($_REQUEST);
            break;
            
            case "search" :
                $this->search($_REQUEST);
            break;
           
            
            case "create" :
                $this->create($_REQUEST);
            break;    
 
            case "detail" :
                $this->detail();
                break;
     
            default:
                $this->index();
                break;
        }
    }
    
    /*Default Admin */ 
    public function index(){
        $buyer_receipt=new Buyer_Receipt($this->Connection);
        $buyer_receipts=$buyer_receipt->getAll();
        $product=new Product($this->Connection);
        $products =$product->getAll();
        $this->view("receipt",array(
            "buyer_receipts"=>$buyer_receipts,
            "products"=>$products,
            "titulo" => "Admin Receipt List"
        ));
    }
    /*Default Buyer  */ 
    
    public function index_entry_by($request){
        $buyer_receipt=new Buyer_Receipt($this->Connection);
        if(isset($request["start_date"]) || isset($request["end_date"])){

            $buyer_receipts=$buyer_receipt->getByEntryDatOrEntryID($request->userid,$request["start_date"],$request["end_date"]);
        }
               
        $buyer_receipts=$buyer_receipt->getByEntry($_SESSION["userid"]);
       
        $product=new Product($this->Connection);
        $products =$product->getAll();

        $div_content ='';
        foreach($buyer_receipts as $buyer_receipt) {
           $div_content .=     
            '<div class="row">
                <div class="col-md-1">'
                .$buyer_receipt["id"] 
                .'<br>'.
                
                $buyer_receipt["buyer"].'</div>
                <div class="col-md-2">'
                .$buyer_receipt["buyer_email"].'
                <br>'.
                $buyer_receipt["phone"] 
                .'</div>
                <div class="col-md-1">'.
                $buyer_receipt["city"] 
                .'</div>
                <div class="col-md-1">'
                .$buyer_receipt["buyer_ip"] 
                .'</div>
                <div class="col-md-1">'
                .$buyer_receipt["receipt_id"] 
                .'</div>
                <div class="col-md-1">'
                   .$buyer_receipt["items"]
                .'</div>
                <div class="col-md-1">'.
                    $buyer_receipt["amount"].
                '</div>
                <div class="col-md-1">'
                .$buyer_receipt["note"]
                .'</div>
                <div class="col-md-1">
                '.$buyer_receipt["entry_at"].
                '</div>
                <div class="col-md-1">'

                 .$buyer_receipt["entry_by"]
                 .'</div>
                <div class="col-md-1">
                <div>
                <a href="index.php?action=detail&id='.$buyer_receipt['id'].'"> class="btn btn-info">Details</a>
                </div>
                </div>
        </div>'; 
        }    
        
        $this->view("buyer",array(
            "buyer_receipts"=>$buyer_receipts,
            "products"=>$products,
            "titulo" => "Buyer Receipt List"
        ));
    }

    /*Search function use for ajax call */ 
    public function search($request){
        $buyer_receipt=new Buyer_Receipt($this->Connection);
        if((isset($request["start_date"]) && $request["start_date"]!='' )  || isset($request["end_date"]) && $request["end_date"]!=''){
           $buyer_receipts=$buyer_receipt->getByEntryDatOrEntryID($request["entry_by"],$request["start_date"],$request["end_date"]);
        }
        else if(isset($request["entry_by"])){
            $buyer_receipts=$buyer_receipt->getByEntry($request["entry_by"]);
        }
        else{
            $buyer_receipts=$buyer_receipt->getAll();
        }
     
        $div_content ='';
        foreach($buyer_receipts as $buyer_receipt) {
           $div_content .=     
            '<div class="row">
                <div class="col-md-1">'
                .$buyer_receipt["id"] 
                .'<br>'
                .$buyer_receipt["buyer"].'</div>
                <div class="col-md-2">'
                .$buyer_receipt["buyer_email"].'
                <br>'.
                $buyer_receipt["phone"] 
                .'</div>
                <div class="col-md-1">'.
                $buyer_receipt["city"] 
                .'</div>
                <div class="col-md-1">'
                .$buyer_receipt["buyer_ip"] 
                .'</div>
                <div class="col-md-1">'
                .$buyer_receipt["receipt_id"] 
                .'</div>
                <div class="col-md-1">'
                   .$buyer_receipt["items"]
                .'</div>
                <div class="col-md-1">'.
                    $buyer_receipt["amount"].
                '</div>
                <div class="col-md-1">'
                .$buyer_receipt["note"]
                .'</div>
                <div class="col-md-1">
                '.$buyer_receipt["entry_at"].
                '</div>
                <div class="col-md-1">'

                 .$buyer_receipt["entry_by"]
                 .'</div>
                <div class="col-md-1">
                <div><a href="index.php?action=detail&id='.$buyer_receipt['id'].'" class="btn btn-info">Details</a>
                </div>
                </div>
        </div>'; 
        }
      print_r($div_content); 
      return $div_content;  
    }
    
    /* Receipt Detail View */
    public function detail(){
        $modelo = new Buyer_Receipt($this->Connection);
        $buyer_receipt = $modelo->getById($_GET["id"]);
        $this->view("detail",array(
            "buyer_receipt"=>$buyer_receipt,
            "titulo" => "Detail Product"
        ));
    }
   /*Validate input according to requirement  */ 
    public function validation($variable){

     $validate_message ="";
     if (!preg_match('/^[A-Za-z0-9 ]+$/u', $variable["buyer"])){
        $validate_message .= 'buyer must contain letters number and space only!<br>';
      }
      if (!filter_var($variable["buyer_email"], FILTER_VALIDATE_EMAIL)) {
        $validate_message .= "Invalid email format<br>";
      }
      if(!is_numeric($variable["phone"]))
      $validate_message .="Phone ". $variable["phone"] . " should be a number<br> ";
    
      if (!preg_match('/^[\p{L} ]+$/u', $variable["city"])){
        $validate_message .= 'city must contain letters and space only!<br>';
      }
      if (!preg_match('/^[\p{L}]+$/u', $variable["receipt_id"])){
        $validate_message .= 'receipt id must contain letters only! No space allowed<br>';
      }
      if(!is_numeric($variable["amount"]))
         $validate_message .="Amount ". $variable["amount"] . " should be a number <br>";
      if(!is_numeric($_SESSION["userid"]))
        $validate_message .="Entry by  should be a number. Exist issue conntact admin <br>";
   
      if (!preg_match('/^[\p{L} ]+$/u', $variable["note"])){
        $validate_message .= 'note must contain letters and space only!';
      }
      /*Check selected items text */
       foreach($variable["items"] as $item) {  
                if (!preg_match('/^[\p{L} ]+$/u',$item)){
                    $validate_message .= $item .' Item must contain letters and space only!';
                }
         
        } 
    return $validate_message;
    }
   
    /* Create a new Receipt from the POST parameters used ajax call
    
    */
   
    public function create($request){
       $validate = $this->validation($request); 
       $item_string="";
       $div_content='';
             
       if($validate!=""){
        $arr =json_encode(array('flag' => 2,'message'=> $validate,"current_buyer_receipt"=>null,"div_content"=>"" )); 
        print_r($arr);
        // not valid return message
        return $arr;
        } 
        elseif(isset($request["receipt_id"]) && $request["receipt_id"]!=''){
            //  create object and set value
            $buyer_receipt=new Buyer_Receipt($this->Connection);
            $buyer_receipt->setCity($request["city"]); 
            $buyer_receipt->setAmount($request["amount"]); 
            $buyer_receipt->setBuyer($request["buyer"]);
            $buyer_receipt->setReceiptId($request["receipt_id"]);
            $count =0;
            $item_string ="";
          // converting multiple item as string separated by space 
            foreach($request["items"] as $item) {
                if($count>0)
                    $item_string .=" ".$item;
                else {
                    $item_string =$item;
                }
                $count= $count+1;
            } 

            $buyer_receipt->setItems($item_string);
            $buyer_receipt->setBuyerEmail($request["buyer_email"]);
            $ip = $_SERVER['REMOTE_ADDR'];
            $buyer_receipt->setBuyerIp($ip);
            $buyer_receipt->setNote($request["note"]);
            $buyer_receipt->setPhone($request["phone"]); 
            $buyer_receipt->setSec(); 
            $buyer_receipt->setEntryBy($_SESSION["userid"]);
            $current_buyer_receipt=$buyer_receipt->save();
            // for buyer user can save next day
            if($_SESSION["is_admin"]==0){ 
                $cookie_name = "userid";
                $cookie_value = $_SESSION["userid"];
                setcookie($cookie_name, $cookie_value, time() + (86400* 1), "/"); // 86400 = 1 day
                    //86400

            } // admin can save after 1 min by refresh
            else { 
                $cookie_name = "adminid";
                $cookie_value = $_SESSION["userid"];
                setcookie($cookie_name, $cookie_value, time() + ( 60 * 1), "/"); // 
                
            }    
            if(!is_null($current_buyer_receipt)){
                $flag= 1;
                $message ="success";
             // div content will be use to prepend save item     
             $div_content .=     
                     '<div class="row">
                         <div class="col-md-1">'
                         .$current_buyer_receipt->id 
                        .'<br>'
                         .$current_buyer_receipt->buyer.'</div>
                         <div class="col-md-2">'
                         .$current_buyer_receipt->buyer_email.'
                         <br>'.
                         $current_buyer_receipt->phone 
                         .'</div>
                         <div class="col-md-1">'.
                         $current_buyer_receipt->city 
                         .'</div>
                         <div class="col-md-1">'
                         .$current_buyer_receipt->buyer_ip 
                         .'</div>
                         <div class="col-md-1">'
                         .$current_buyer_receipt->receipt_id
                         .'</div>
                         <div class="col-md-1">'
                            .$current_buyer_receipt->items
                         .'</div>
                         <div class="col-md-1">'.
                             $current_buyer_receipt->amount.
                         '</div>
                         <div class="col-md-1">'
                         .$current_buyer_receipt->note
                         .'</div>
                         <div class="col-md-1">
                         '.$current_buyer_receipt->entry_at.
                         '</div>
                         <div class="col-md-1">'
         
                          .$current_buyer_receipt->entry_by
                          .'</div>
                         <div class="col-md-1">
                         <div>
                         <a class="btn btn-info" href="index.php?action=detail&id='.$current_buyer_receipt->id.'" >Details</a>
                         </div>
                         </div>
                 </div>'; 
            }
           else { // no object returned means save failed
                $flag=0;
                $message ="failed";
                $div_content=""; 
           }
           
        } else{  // receipt id not set 
            $flag=0;
            $message ="receipt id not set";
            $current_buyer_receipt=null;
           $div_content=""; 
        }

        $arr =json_encode(array('flag' => $flag,'message'=> $message,"current_buyer_receipt"=>$current_buyer_receipt,"div_content"=>$div_content )); 
        print_r($arr);
        return $arr;
    }


    
   /**
    * Create the view that we pass to it with the indicated data.
    *
    */
    public function view($vista,$datos){
        $data = $datos;  
        require_once  __DIR__ . "/../view/" . $vista . "View.php";
        // define view method with file name formating
    }

}
?>

<?php 
class LoginController{

    private $conectar;
    private $Connection;

    public function __construct() {

        if(isset($_SESSION["userid"]) && trim($_SESSION["userid"])!='' ){
            header('Location: /mvc/index.php');
        }
        require_once  __DIR__ . "/../core/Conectar.php";
        require_once  __DIR__ . "/../model/user.php";
        $this->conectar=new Conectar();
        $this->Connection=$this->conectar->Connection();
    }

    public function run($action){
        switch($action)
        { 
            case "index" :
                $this->index();
                break;
            case "verfication" :
                $this->verfication();
            break;
            case "logout" :
                $this->logout();
            break;
            case "detalle" :
                $this->detalle();
            break;
       
            default:
                $this->index();
                break;
        }
    }
    

    public function index(){
        $this->view("login",array(
            "titulo" => "Login"
        ));
    }


    public function detalle(){
        
     
        $modelo = new User($this->Connection);
        $user = $modelo->getById($_GET["id"]);
        $this->view("detalle",array(
            "user"=>$user,
            "titulo" => "Detail User"
        ));
    }
    
  /*Verification method verify user set session with admin or regular previlige*/
    public function verfication(){
        if(isset($_POST["email"])&& isset($_POST["password"])  ){
            $modelo = new User($this->Connection);
            $user = $modelo->getByEmailPassword($_POST["email"],$_POST["password"]);
            // 
            print_r($user);
            if(isset($user) && !is_null($user))
            {
            print_r("welcome ".$user->name );
            $_SESSION["username"]=$user->name;
            $_SESSION["userid"]=$user->id;
            $_SESSION["useremail"]=$user->email;
            $_SESSION["is_admin"]=$user->is_admin;

                header('Location: /mvc/index.php');
            }
            else 
            {
                header('Location: /mvc/login');
                print_r("Authentication Failed");

            }
            

        }
    }

    /*Logout method unset session variable*/
    public function logout(){
            if(isset($_SESSION["userid"]))
            {
                unset ($_SESSION["username"]);
                unset($_SESSION["userid"]);
                unset($_SESSION["useremail"]);
                unset($_SESSION["is_admin"]);

            }
            header('Location: /mvc/login');
    }



    
   
    public function view($vista,$datos){
        $data = $datos;  
        require_once  __DIR__ . "/../view/" . $vista . "View.php";

    }

}
?>

<?php
class Buyer_Receipt {
    
    private $table = "buyer_receipts";
    private $Connection;

    private $id;
    private $amount;
    private $buyer;
    private $receipt_id;
    private $items;
    private $buyer_email;
    private $buyer_ip;
    private $note;
    private $city;
    private $phone;
    private $sec;
    private $hash_key;
    private $entry_at;
    private $entry_by;

    public function __construct($Connection) {
		$this->Connection = $Connection;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    public function getAmount() {
        return $this->amount;
    }

    public function setAmount($amount) {
        $this->amount = $amount;
    }

    public function getBuyer() {
        return $this->buyer;
    }

    public function setBuyer($buyer) {
        $this->buyer = $buyer;
    }

    public function getReceiptId() {
        return $this->receipt_id;
    }

    public function setReceiptId($receipt_id) {
        $this->receipt_id = $receipt_id;
    }

    public function getItems() {
        return $this->items;
    }

    public function setItems($items) {
        $this->items = $items;
    }


    

    public function getBuyerEmail() {
        return $this->buyer_email;
    }

    public function setBuyerEmail($buyer_email) {
        $this->buyer_email = $buyer_email;
    }

    public function getBuyerIp() {
        return $this->buyer_ip;
    }

    public function setBuyerIp($buyer_ip) {
        $this->buyer_ip = $buyer_ip;
    }

    public function getNote() {
        return $this->note;
    }

    public function setNote($note) {
        $this->note = $note;
    }

    
    public function getCity() {
        return $this->city;
    }

    public function setCity($city) {
        $this->city = $city;
    }

    
    public function getPhone() {
        return $this->phone;
    }

    public function setPhone($phone) {
        $phone_with_880 = preg_replace('/^0/', '880', $phone);
        $this->phone = $phone_with_880; // if it open with 0 
    }


    public function getSec() {
        return $this->sec;
    }

    public function setSec() {
        $this->sec = bin2hex(random_bytes(5)); //generate random data for salt creation
    }

    public function getHash($hash_key) {
       return $this->hash_key;
    }
    public function setHash($receipt_data,$salt) {
        
        $binary = false;
        $reciept_with_salt=$receipt_data.$salt;
        $this->hash_key = hash(
            'sha512',
            $reciept_with_salt,
            $binary
           ); 
    }

    public function getEntryBy() {
        return $this->entry_by;
    }

    public function setEntryBy($entry_by ) {
        $this->entry_by  = $entry_by ;
    }

    public function getEntryAt() {
        return $this->entry_at;
    }

    public function setEntryAt($entry_at) {
        $this->entry_at  = $entry_at ;
    }

    public function save(){
     //   $this->setSec(); //Random generator
       
        $receipt_data= $this->receipt_id;
        $salt = $this->buyer_email. $this->sec;    
 
        $this->setHash($receipt_data,$salt); // creating 

        //$hashed_reciept= 
        //echo $this->city;
        // $this->city ="CHiPiis"; 
        $consultation = $this->Connection->prepare("INSERT INTO " . $this->table . " (amount,buyer,
        receipt_id,items,buyer_email,buyer_ip,note,city,phone,sec,hash_key,entry_by)
                                        VALUES (:amount,:buyer,:receipt_id,:items, :buyer_email,
                                       :buyer_ip,:note,:city,:phone,:sec,:hash_key,:entry_by )");
        
        
        $result = $consultation->execute(array(
            
            "amount" => $this->amount, 
            "buyer" => $this->buyer,
            "receipt_id" => $this ->receipt_id,
            "items" => $this->items,
            "buyer_email" => $this->buyer_email,
            "buyer_ip" => $this->buyer_ip,
            "note" => $this-> note,
            "city" => $this->city,
            "phone" => $this->phone,
            "sec"=> $this->sec,
            "hash_key"=> $this->hash_key,
             "entry_by"=> $this->entry_by
            
        ));
        if($result==1){
        $this->id= $this->Connection->lastInsertId();
      
        $current_entry = $this->getById($this->id);
        
        return $current_entry;
    }
        else {
            return null;
        } 

    }

    public function update(){

        $consultation = $this->Connection->prepare("
            UPDATE " . $this->table . " 
            SET 
                buyer = :buyer,
                items = :items, 
                phone = :phone
            WHERE id = :id 
        ");

        $resultado = $consultation->execute(array(
            "id" => $this->id,
            "buyer" => $this->buyer,
            "items" => $this->items,
            "phone" => $this->phone
        ));
        $this->Connection = null;

        return $resultado;
    }
        
    
    public function getAll(){

        $consultation = $this->Connection->prepare("SELECT * FROM " . $this->table .' order by id DESC');
        $consultation->execute();
        /* Fetch all of the remaining rows in the result set */
        $resultados = $consultation->fetchAll();
        $this->Connection = null; //cierre de conexión
        return $resultados;

    }
    
    
    public function getById($id){
        $consultation = $this->Connection->prepare("SELECT * FROM " . $this->table . "  WHERE id = :id");
        $consultation->execute(array(
            "id" => $id
        ));
        /*Fetch all of the remaining rows in the result set*/
        $resultado = $consultation->fetchObject();
        $this->Connection = null; //connection closure
        return $resultado;
    }
    
    public function getBy($column,$value){
        print_r($column);
        print_r($value);
        $consultation = $this->Connection->prepare("SELECT * FROM " . $this->table . " WHERE :column = :value");
        $consultation->execute(array(
            "column" => $column,
            "value" => $value
        ));
        $resultados = $consultation->fetchAll();
        print_r($resultados);   
        $this->Connection = null; //connection closure
        return $resultados;
    }
    public function getByEntry($value){
        $consultation = $this->Connection->prepare("SELECT * FROM " . $this->table . " WHERE entry_by = :value order by id desc");
        $consultation->execute(array(
            "value" => $value
        )); 
        $resultados = $consultation->fetchAll();
//        print_r($resultados);   
        $this->Connection = null; //connection closure
        return $resultados;
    }
public function getByEntryDatOrEntryID($value,$sdate,$edate){
  
    $use_and =0; // dynamically conacanate AND
    $query_string = "SELECT * FROM " . $this->table; 
    $sub_query_string ="";
    if(isset($value) && is_numeric($value)) {
     $arr["value"] = $value;
     $use_and =1;
     $sub_query_string =" WHERE entry_by = :value";
    }
   if(isset($sdate) && $sdate!=''){
    $arr["sdate"] = $sdate;
        if( $use_and==1){
            $sub_query_string .= " AND entry_at >= :sdate";
            $use_and=1;
        }
        else {
            $sub_query_string =" WHERE entry_at >= :sdate";
            $use_and=1;
        }
   }
   if(isset($edate) && $edate!=''){
    $arr["edate"] = $edate;
        if( $use_and==1){
        $sub_query_string .= " AND entry_at <= :edate";
        $use_and=1;
        }
        else {
            $sub_query_string =" WHERE entry_at <= :edate";
            $use_and=1;
        }
   

    }
    $sub_query_string .=" order by id desc";
$consultation = $this->Connection->prepare($query_string.$sub_query_string);

/*$consultation = $this->Connection->prepare("SELECT * FROM " . $this->table . " WHERE entry_by = :value 
    AND entry_at >= :sdate AND entry_at <= :edate
    ");
  */    $consultation->execute($arr);
        $resultados = $consultation->fetchAll();
//        print_r($resultados);   
        $this->Connection = null; //connection closure
        return $resultados;

    }




    
    public function deleteById($id){
        try {
            $consultation = $this->Connection->prepare("DELETE FROM " . $this->table . " WHERE id = :id");
            $consultation->execute(array(
                "id" => $id
            ));
            $Connection = null;
        } catch (Exception $e) {
            echo 'Failed DELETE (deleteById): ' . $e->getMessage();
            return -1;
        }
    }
    
    public function deleteBy($column,$value){
        try {
            $consultation = $this->Connection->prepare("DELETE FROM " . $this->table . " WHERE :column = :value");
            $consultation->execute(array(
                "column" => $value,
                "value" => $value,
            ));
            $Connection = null;
        } catch (Exception $e) {
            echo 'Failed DELETE (deleteBy): ' . $e->getMessage();
            return -1;
        }
    }
    
}
?>

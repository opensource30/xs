<?php
class Product {
    private $table = "products";
    private $Connection;

    private $id;
    private $item_name;
    private $created_at;


    public function __construct($Connection) {
		$this->Connection = $Connection;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    
    public function getItemName() {
        return $this->item_name;
    }

    public function setItemName($item_name) {
        $this->item_name = $item_name;
    }

   

 
    public function save(){
        $consultation = $this->Connection->prepare("INSERT INTO " . $this->table . " (item_name)
                                        VALUES (:item_name)");
        $result = $consultation->execute(array(
            "item_name" => $this->item_name,
           
        ));
      
        $this->Connection = null;

        return $result;
    }

   
        
    
    public function getAll(){

        $consultation = $this->Connection->prepare("SELECT * FROM " . $this->table);
        $consultation->execute();
        /* Fetch all of the remaining rows in the result set */
        $resultados = $consultation->fetchAll();
        $this->Connection = null; //cierre de conexión
        return $resultados;

    }
    
    
    public function getById($id){
        $consultation = $this->Connection->prepare("SELECT * FROM " . $this->table . "  WHERE id = :id");
        $consultation->execute(array(
            "id" => $id
        ));
        /*Fetch all of the remaining rows in the result set*/
        $resultado = $consultation->fetchObject();
        
        $this->Connection = null; //connection closure
        return $resultado;
    }

    
    public function deleteById($id){
        try {
            $consultation = $this->Connection->prepare("DELETE FROM " . $this->table . " WHERE id = :id");
            $consultation->execute(array(
                "id" => $id
            ));
            $Connection = null;
        } catch (Exception $e) {
            echo 'Failed DELETE (deleteById): ' . $e->getMessage();
            return -1;
        }
    }
    
  
    
}
?>

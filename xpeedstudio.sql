-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2022 at 07:12 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xpeedstudio`
--

-- --------------------------------------------------------

--
-- Table structure for table `buyer_receipts`
--

CREATE TABLE `buyer_receipts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `amount` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `buyer` varchar(255) NOT NULL,
  `receipt_id` varchar(20) NOT NULL,
  `items` varchar(255) DEFAULT NULL,
  `buyer_email` varchar(50) NOT NULL,
  `buyer_ip` varchar(20) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `city` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `sec` varchar(20) NOT NULL,
  `hash_key` varchar(255) NOT NULL,
  `entry_at` date NOT NULL DEFAULT current_timestamp(),
  `entry_by` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `buyer_receipts`
--

INSERT INTO `buyer_receipts` (`id`, `amount`, `buyer`, `receipt_id`, `items`, `buyer_email`, `buyer_ip`, `note`, `city`, `phone`, `sec`, `hash_key`, `entry_at`, `entry_by`) VALUES
(172, 2820, 'SAMON', 'ASBBA', 'watch pen', 'samon@gmail.com', '::1', 'Test Text', 'Chittagong', '8801776220433', 'ec8fcd228d', '7f6ce9138e764c644ee001734c66c9bf8cfdcc6cf4ce1087ff99680a48e3816e92e0a779d919a35f1b7bce2edb266543f82129eb3535dd16c5cb16365c139e56', '2022-06-15', 1),
(173, 2820, 'LAMON', 'TASBBA', 'watch camera', 'komak@gmail.com', '::1', 'Test normal buyer', 'Chittagong', '8801776220433', 'ad6fa4d766', '15e6c43e3b7acb1c41392079b026ae9b633d27a464683d3f10d165c331c94885038b53219ea50b60936df5332115c68867b3c13b0ae321dd70c41128dca4dd78', '2022-06-16', 3),
(174, 2820, 'SUION', 'XYZASAD', 'watch camera', 'jama@gmail.com', '::1', 'Lovely day', 'Chittagong', '8801776220433', '9ef16a5bb3', 'e7c5c98e8e99fc7ade34c9fd3f88a7141a0df90c52067c8a48aa56a03f5a132ee9d30bf77b33e8ac011de86b17a05cd3b513db5852dc577ce0efd6a22a793b76', '2022-06-16', 4),
(175, 19292, 'Monon', 'ASBBA', 'watch shoe pen camera', 'monon@gmail.com', '::1', 'sddsdds dssd', 'Rangpur', '8801876220433', '20dd78f05a', '285df2fc56c6b0724989305c375d56388eef4a7735c286ad7ae179fc3439ca33aed6293d9f3a68d09db486af20a545720dbc024ea7079c5dd44e0dfb7c9b3a3b', '2022-06-16', 2),
(176, 5000, 'Tomtom212', 'ATBBA', 'watch camera', 'gaembiet@gmail.com', '::1', 'I am happy', 'Barisal', '8801976220433', 'a79052c82b', '63fc11595eb230485a763196d46f1ff489a3b1689171cce97adc7da29628e4a6c700ead8be3994249f09cb16747dc4b417176b062f69f4771e29ca842c888986', '2022-06-16', 3),
(177, 67717, 'Adam', 'XTYYUA', 'watch shoe camera', 'rajib.2002bd@gmail.com', '::1', 'wonder ful', 'Dhaka', '8801776220433', 'fde5f5ab8f', '4b39aef1ea6cdb9521df74923055ad28afb15bf36f92f1131e5390c462b487457a4f56952572ee5e1ff3868e5b6fa99d26f0315146bd91c0b65e3a41942e6b29', '2022-06-16', 1),
(178, 89281, 'TY1 YY', 'YYYYsha', 'shoe', 'tayay@gmail.com', '::1', 'I am wondering why ', 'china town', '8801926662159', 'a269e3a0f9', 'd93346cd5b7cdafb1d01784443e5cce980c53f2c62027e95efa568792c25ca96f947b6c4364e9c299007df07bb5120fe1ac0f8f360f171e6dbb009449bd98179', '2022-06-16', 2);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `Surname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `sec` varchar(20) NOT NULL,
  `hash_key` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `Surname`, `email`, `phone`, `sec`, `hash_key`) VALUES
(1, 'aট্রেনে ভারতে যাবে পণ্য', 'Chowdhurya', 'rajib.2002bd@gmail.com', '01926662150', '', ''),
(2, 'Rajib Chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01926662150', '', ''),
(3, 'Rajib ChowdhuryX', 'Chowdhury', 'rajib.2002bd@gmail.com', '01926662150', '', ''),
(4, 'Rajib Chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01926662120', '', ''),
(5, 'Lovely', 'akhter', 'lovely@gmail.com', '012818218', '', ''),
(6, 'Rajib Chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01926662150', '', ''),
(7, 'Rajib Chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01926662150', '', ''),
(8, 'Rajib Chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01926662150', '', ''),
(9, 'Rajib Chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01926662150', '', ''),
(10, 'Rajib Chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01926662150', '', ''),
(11, 'Rajib Chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01926662150', '34b5b0cd32', '49f166ca9c3c07c8a558abbf1f1ccdf5916b3b070378614ecc6eb0b82873ee48427f20771e2054167be61b5b96d6686cd06c4ce1e4cf082c59e43604ec54d1f6'),
(12, '', '', '', '', '767a8e5316', '6a93d94eaf1401931b5f2ebe709576141a66bb85d2616acae65af9c197be2afd58b84bbf3dee9d1d90694c33f2cc59e0b2eb3c02b4aa5632160b7a223cc08a2d'),
(13, '', '', '', '', 'fcbdfa7212', 'b9593513e7ad8337d4214fc85d08208d79a1a96a19a0ae8948ea471dfbe6c942cd1a3fd5ddc82c4b60762b1eae1f582ae08c4c4e42ff1cb65a943ee7c916a7ba'),
(14, 'Rajib Chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01926662150', 'fefc690b33', 'ae1aadd693b3f0a484817cdb745495b2f34230d2ab2f09b696463197bca95022729d0bc2c68d31608e091e57e9bd3547e0a85922f78dd06d5546bbdddb93aec2'),
(15, 'rajib chowdhury', 'Chowdhury wOw ghor', 'rajib.2002bd@gmail.com', '01776220433', '2910906e27', '71aa71aac0e3876f86ab7dbcc8818551095db7501106c01b6d3d8450defed96d19f73db07e0f6d925f3ca81912a556c6fc8cf4bd213ccb1d3a30b3319dd86e0e'),
(16, 'rajib chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01776220433', 'bfd0674056', '9ad7e6fe1b09851f06a0a605153a99161773bb570089297189d6865fb074ac3aa85f4d58c667e161fef01377bbb488e6dee51edc8403e38dd44fed33ee40abbe'),
(17, 'rajib chowdhury', 'Chowdhury wOw ghor', 'rajib.2002bd@gmail.com', '01776220433', 'b1db3fbe62', 'abfc5e285dfe3c0f2f426d00b6782190c4ab6a8e90089b337d01de6c211ffa08560c08b42def210a7787fbc717f1d25698a4fa1cc0d4ee1fac2acb64e7f04716'),
(18, 'rajib chowdhury', 'Chowdhury wOw ghor', 'rajib.2002bd@gmail.com', '01776220433', '50ffe6aaa4', '4f24f0f4a7b818699664591cc75657c10d4353ea54a358f344465728750dbda72c244b946dbf6f876c78cc059a5a5ebf02af32b59ab132d266a01fb34c33557f'),
(19, 'rajib chowdhury', 'Chowdhury wOw ghor', 'rajib.2002bd@gmail.com', '01776220433', 'e318102548', '13260a695a0bd34925ed04b917669246bcd723268ed98927b9b9dad7e839e5966660387957db1dd60ff0ba27d0bb6ca9b2cbfc9f13eb90d532ddb300ab5d7aa9'),
(20, 'rajib chowdhury', 'Chowdhury wOw ghor', 'rajib.2002bd@gmail.com', '01776220433', '40307cc3e9', '572bc532a560d980d312b351b9d144c1f41362abe0c5ef8f99cf77589b9a19d31688e7974b5a7ebdf5fff098c9feb6ff4d5eefa2fd00da1377c34ab3513442e0'),
(21, 'rajib chowdhury', 'Chowdhury wOw ghor', 'rajib.2002bd@gmail.com', '01776220433', '3d72e4b15b', 'be70e79ad552c2e11080dc94c91de59ad317d58f6912eebe993010eba2e94db6a3feaac5af0a7efa49477b46743ffa7d97a90a2a75eebb0c0659d1b846e08d9b'),
(22, 'rajib chowdhury', 'Chowdhury wOw ghor', 'rajib.2002bd@gmail.com', '01776220433', '6f10ea4d89', '7fecc61721d8056021cb27b5dc0cd6bcf3e93e9fa335e39b9b0b2353c97991686b4e048ca0a6db99442d8775e0af907b140c6020904d2217f036f89d4dcc42bc'),
(23, 'rajib chowdhury', 'Chowdhury wOw ghor', 'rajib.2002bd@gmail.com', '01776220433', 'd9b02834ab', 'a010a7b5d7dc2e0ecb18d9fccb2a607dec99ae87b4f157528fea5ca18e41ab200253e2f61c62e9cef39389ec9fbd8c05642b981473288a3303c49bdffbbc5ae4'),
(24, 'rajib chowdhury', 'Chowdhury wOw ghor', 'rajib.2002bd@gmail.com', '01776220433', '89ac4d1c6f', 'a935e048305ec8fbc2479f065842ae4aa1109e56cfd9285857a7c6b5d9a9ce150948566472d9fbf15916c947a1b54050d43611f5b945a3af0332d9e0d9946060'),
(25, 'rriyona', 'asa', 'rajib.2002bd@gmail.com', '01776220433', '312e923010', '7f81ce20668013e9fd0d8f86e30868b356fe06f7b33f1f3a31407c5ba18a24cd74eeb635e33b13c99b0b60ec8df76046fa5c054a0930a509dcaea8aad7ba03ce'),
(26, 'rajib chowdhury', 'sasa', 'rajib.2002bd@gmail.com', '01776220433', '1eb094acbe', 'ff0cd69222691e35340fa1b05a39959d525366bbd430bdb62b09d805b4d27c8a48dd661be0e636d8f2d41c39409e9c70ed1ce0c8434a5c27188a36711e73b777'),
(27, 'awdhury', 'sasa', 'rajib.2002bd@gmail.com', '01776220433', '891afd0187', '882799ffb72618b36e898047cfcbbd18a8b971c09a73ca899c8645075f8f63a331b13467dc38006f2b8b16051a8f4239f4cfb8a4fbe6e7b561b813ec4e2c5a73'),
(28, 'rajib chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01776220433', '567fc905cc', 'ddc125796543f66a4a94be73bc4b5ee60f7609e46dc5190bf85bfce12f29044a37f9b2dce456e810c6f8131dd5c49935d303205537bc6a12a4af71997a872327'),
(29, 'rajib chowdhury', 'Mogambo khos hogiya', 'rajib.2002bd@gmail.com', '01776220433', '96dcabe66e', 'f90724268cd2e6b4d4d06f9d002299e7ea42620013da58ea3937eee6f38e80b58cdebb8214e4d83f8e0aae56b39d0470c054260a00219960856a559ab22ab394'),
(30, 'rajib chowdhury', 'sds', 'rajib.2002bd@gmail.com', '01776220431', '7d7c97558b', 'b9a9c9883afbae7f0e68e68ceac5c626ab8158e1dbaa066d7eff7463ac1444ff01a54684d3f7c62ac348e11320d35f810f115ab34ed1b6842aacbae725659007'),
(31, 'dhury', 'sds', 'rajib.2002bd@gmail.com', '01776220431', 'a48aa311e4', '453f30efb091e5b638150373f65b592816f97b40c04d8493aff67e2f1d2fc1470dd268ed9db4889be2d1f5ce6acb86f8d86612446fc16ebb143db3a58792dbc1'),
(32, 'Y', 'keno', 'rajib.2002bd@gmail.com', '01776220433', 'd081ad33ba', 'bd96ddf382fcaed5c4824e36a71b00446c281875a3be5932fc28bf8930b02d780e40a7d7a884800159d69b123e35a392641ee276ca7bf897e1390b38d9cb6e35'),
(33, 'rajib chowdhury', 'sd', 'rajib.2002bd@gmail.com', '01776220433', 'a2bf45f547', '17a4d9cc3da1c3bcb9bd0bb0b910068eb7b5f0affb0c31e5548c2d2176f6bc934953d6e134150feb84c4543e89df0944353f749f6e8a27bdb574f0a303bb28dd'),
(34, 'sds', 'sdsd', 'dss', '1211', '915a43fda5', '1ff6e736cd3e644eaf5e957c8d83f7a623e3fc40af00b1afb8599c48c9daafbe8b3802bbf0a89e5fb66a92ea8dcb531a4951aada964f9648ca706623afec0e8a'),
(35, 'sds', 'sdsd', 'dss', '1211', '7c503a2143', '1e00fdae25dccf92b58c53e65e6788cceffb02b7156d6cb07e8520ece2c2932e6e4d77cbd2389f839b4155cb9b750a1d8effd570629fbf838a867bf6ae7dc334'),
(36, 'sds', 'sdsd', 'dss', '1211', '78f4a6fbca', '81fa99b2cd8d4a90ead297a2376f48fcbb68a64f1768c7adcffeb8afd531e7ed2b1741965046d00a2888a3f94972fe28760647450402ed2cc1725639568fb5ae'),
(37, 'rajib chowdhury', 'dsdfs', 'rajib.2002bd@gmail.com', '01776220433', 'cb77ddda44', '56655b26f237f58f1e04f3802254df4ab3757cc378688f0838f4c3189d57cbb1383175974ecbbc1ec5129d33a8216d68f2f4cbaaa0280dd0bb47fb60c14f2fe2'),
(38, 'x', 'Chowdhury', 'rajib.2002bd@gmail.com', '017762120433', '4da31fa2e9', '4689cc99e48f61d77207c3cd6fb672561f120b8dd81efa7b6e1de2aa6f619cf1184ea0f41f910ed4b029880b641b7ef209ac3a00f156f5a46e25f3a4daf3c57e'),
(39, 'x', 'Chowdhury', 'rajib.2002bd@gmail.com', '017762120433', '8fd48b44b7', '85d250efc1c0a84d9bba08001089d90f5e1e1e36883b8e8fd63608727da0cd46e58f1fa74a207135425cd23f00a1d41dc65be8727530cf1beae3b1afaa20961e'),
(40, 'sd', 'ds', 'dsf', 'dsf', 'c32448fd96', '7bf24003c0457ef32308edf9270f215ae2c09281f55cc2adcc86619a2557373e52657c03e306742064d1e1e854aacd74542f53ebb60be9fee867be0e3cb60497'),
(41, 'rajib chowdhury', 's', 'rajib.2002bd@gmail.com', '01776220433', '27fae543b9', 'fcedc16ccecb83d58df51afd424440dcf1a34877326434fe7f60e08115c19aa31d805b874c50d46e42f892b64f00b8922972fc4a86b8fc7354d208c4ecc9ecb4'),
(42, 'Qchowdhury', 'S', 'rajib.2002bd@gmail.com', '01776220433', '6cf7289b88', '8d1cb9fd176b6412f532e34946f43dd017b7cfe13d44d614d054fa917c1741cea5177b72ac7226ceaf027d201822d0dd360900e39b793599836a51e5cd104d49'),
(43, 'rajibx chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01776220433', 'ccf25ac297', '71df25820fc3033983772416079017f4407d7441badf8a25f1394bbe09a936a1207eb961d9c6388f679113608fff40f2073b80fa37c157263b190839fe277d7e'),
(44, 'rajibx chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01776220433', 'c4194d32a2', 'bef962d530e2bf0da337039bf7a942853ac8eee05d6623d0e8ba13b60fa0e3c53ff4ebf3b13c8fe5be201c4e2027ec85828e4a7110cf3e245767c3e1d0ac8026'),
(45, 'rajibx chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01776220433', '5b3a8cdd54', 'bab4b447e7aa3ea7052d56990f66ea0185cc568b0e08ff222ab6fe2e14579d36724894a99deb63fe21dd23b8ada7dc0d50fbcddc148123c12ab8d83b111690a8'),
(46, 'rajibx chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01776220433', '8105ae0feb', 'adbe43db43ef754df61bfceecf32d79e4a40ceee0097c838151d4b5d7c46d8067c51e39ddc0cfcf9d33810af57f7bbfb3639d787616dd5140a28dd3b45f1bada'),
(47, 'rajibx chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01776220433', '9ac9ae0df1', 'e9f39c601041a2485702533ed1852f723ba070ff037d74144272b3a9dffa68e20015149f86768382a56e8421bec0eaf8c70894181e25d3cb5ef66b2024742347'),
(48, 'rajibx chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01776220433', 'afc0dc23fc', '469b5e06ad591081bb9dbc474d0d2d07dedcc4288a0b1917e6532a971c92c796bf0497a3c7856f46a927414b255c6cc3933e35712636a2fe41b44743a9e38aa0'),
(49, 'asas', '121', 'rajib.2002bd@gmail.com', '0177620433', '129fedaafd', '1ea1e5b37d9b742fc51f2e740e6e41779cd157bae499e2f603123bb596166447fa588be0ede879c6f60971dc2a8bbd95a9314d4cb3eac548abdd380993bd0343'),
(50, 'asas', '121', 'rajib.2002bd@gmail.com', '0177620433', '538dc5f201', '2796cb9c5e995b7724f1099e70d7e56b51e80306da504d4d2d981b8ca365c909948354050d274c0ad32effa1179cc8988a64ac930743bc46bcdfd1b67bfe56f9'),
(51, 'asas', '121', 'rajib.2002bd@gmail.com', '0177620433', 'ce2fe8d3b9', '140e8886d5a0011d8e78edd0183421ef1654badd4ec3c14b38f5173cdb52260a012d0bf9b14594e61b21368839638cd22a66480f74fe5c9ae999ff151d16989c'),
(52, 'asas', '121', 'rajib.2002bd@gmail.com', '0177620433', '90aed677ea', 'd2f2e8a6ca0366bb0f8ede2bb53d57fc2979d862766af2155907f83686601557483a53a618309aa99d3f0e5ee9befc5faa54f3c3441cf4d5616cc1975d5a7603'),
(53, 'asas', '121', 'rajib.2002bd@gmail.com', '0177620433', '63f3d79e48', '814bf40b514890da08eff1291a38076e48236c002905bcab3e98140c5e834d9c640b97666452e29f8e7f014eefb9e2c001dbfc99fefeecac71154837b862f40b'),
(54, '202006151612MK', 'Chowdhury', 'rajib.2002bd@gmail.com', '01776220433', 'ca7a821e32', '50a297c43fef4c61e3ae1e5ce69508887379a03c1d9bfa5d3639fdd5859d3b53acd7cf605f53c8f397e61140fe3bcd885596bab37f03a3a99ea23bde0a544272'),
(55, 'ajib chowdhury', '', 'rajib.2002bd@gmail.com', '01776220433', '85747781ce', 'afd78055ceac4e242d83700ef19ee2dd89a7dc84f600b38a758b0876ed1a97267c7d707514a14c778851b310a5dd1d7182ef26b78c1b3bb60fbdaea592983a19'),
(56, 'rajib chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01776220433', '54b915bf81', '3f25f56c3a2d5a018ac52ea240e8afd576eb6ae77c57227376c9c5d4b8fc79a03a14d6452ed9c96a62a0da045640f95aaf9a66943d8b768f4e68fd63af680cfc'),
(57, 'rajib chowdhury', 'Chowdhury', 'rajib.2002bd@gmail.com', '01776220433', '14f15f2ca6', '9860dd09faa3bc460f38c5a6ae749e9f3f9e81705dc7baa481996df4bf6ac8691a0590edadde4812db94154f6b088d966b810a2a03e2ffcf2939397330a0acf7');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `item_name` varchar(80) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `item_name`, `created_at`) VALUES
(1, 'watch', '2022-06-15 12:13:31'),
(2, 'shoe', '2022-06-15 12:13:31'),
(3, 'pen', '2022-06-16 21:18:36'),
(4, 'camera', '2022-06-16 21:18:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `is_admin`, `created_at`, `updated_at`) VALUES
(1, 'Rajib', 'rajib.2002bd@gmail.com', '01fbebafbc5a7f5c0d8be75af180301e', 1, '2022-06-16 15:16:13', NULL),
(2, 'quddus', 'quddus@gmail.com', '01fbebafbc5a7f5c0d8be75af180301e', 0, '2022-06-16 15:16:30', NULL),
(3, 'Jamal', 'jamal@gmail.com', '01fbebafbc5a7f5c0d8be75af180301e', 0, '2022-06-16 15:16:39', NULL),
(4, 'kamal', 'kamal@gmail.com', '01fbebafbc5a7f5c0d8be75af180301e', 0, '2022-06-16 15:17:17', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buyer_receipts`
--
ALTER TABLE `buyer_receipts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `buyer_receipts_entry_by_foreign` (`entry_by`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buyer_receipts`
--
ALTER TABLE `buyer_receipts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `buyer_receipts`
--
ALTER TABLE `buyer_receipts`
  ADD CONSTRAINT `buyer_receipts_entry_by_foreign` FOREIGN KEY (`entry_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

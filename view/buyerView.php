<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>Receipt View</title>
        <link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/dist/css/custom.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

    </head>
    <body>
    <div class="container">        
        
        <div class="row"> 
            <div class="col-lg-5 mr-auto" id="add">
                <form action="" method="post" id="rform">
                    <h3>Add Receipt</h3>
                    <hr/>
                    Buyer: <input type="text" name="buyer" id="buyer" class="form-control" value=""/>
                    Buyer Email: <input type="email" name="buyer_email" id="buyer_email" value="" class="form-control"/>
                    Phone: <input type="text" name="phone" id="phone" value="" class="form-control"/>
                    City: <input type="text" name="city" id="city" value="" class="form-control" />
                    Receipt ID: <input type="text" name="receipt_id" id="receipt_id" value="" class="form-control"/>
                    Amount: <input type="text" name="amount" id="amount" class="form-control" value=""/>
                    Items: 
                        <select class="selectpicker" multiple data-live-search="true" name="items[]" id="items">
                        <?php foreach($datos["products"] as $products) {  
                            echo "<option>".$products["item_name"]."</option>";
                          } ?>
                        </select>
                    <br>        
                    Notes: 
                    <textarea name="note" id="note" rows="8" cols="100"></textarea>
                    
                    <?php if(!isset($_COOKIE['userid'])) {
                    echo '<input type="button" value="Save" class="call-btn" id="savebtn"/>';
                    echo '<input type="button" value="You can save Later" class="inactive-btn" style="display:none;"/>';
                
                    } else if($_COOKIE['userid']!=$_SESSION['userid']) { // means cookie of another sign in suser
                      echo '<input type="button" value="Save" class="call-btn" id="savebtn"/>';
                      echo '<input type="button" value="You can save Later" class="inactive-btn" style="display:none;"/>';

                    } else {
                        echo '<input type="button" value="Save" class="call-btn" id="savebtn" style="display:none;"/>';
                        echo '<input type="button" value="You can save Later" class="inactive-btn"/>';

                    }
                  
                  ?> 
                    

                </form>

                <div id="message" style="color:red;"></div>
            </div>    
            <div class="col-lg-1">  <a href="/mvc/logout">Log Out</a></div>

            <div class="col-lg-12">
                <div class="container">
                   <div class="row">
                        <div class="col-md-1">
                            ID. Buyer
                        </div>
                        <div class="col-md-2">
                            Email And Phone
                        </div>
                        <div class="col-md-1">
                               City 
                        </div>
                        <div class="col-md-1">
                            IP 
                        </div>
                        <div class="col-md-1">
                            Receipt ID
                        </div>
                        <div class="col-md-1">
                            Items
                        </div>
                        <div class="col-md-1">
                            Amount
                        </div>
                        <div class="col-md-1">
                            Note
                        </div>
                        <div class="col-md-1">
                            Entry At
                        </div>
                        <div class="col-md-1">
                               Entry By
                        </div>
                        <div class="col-md-1">
                            Show Details
                        </div>
                    </div>

                    <div id="dynamic">
                        <?php foreach($datos["buyer_receipts"] as $buyer_receipt) {?>
                   
                        <div class="row">
                            <div class="col-md-1">
                            <?php echo $buyer_receipt["id"]; ?> 
                            <?php echo $buyer_receipt["buyer"]; ?> 
                            </div>
                            <div class="col-md-2">
                            <?php echo $buyer_receipt["buyer_email"]; ?>
                            <br>
                            <?php echo $buyer_receipt["phone"]; ?> 
                            </div>
                            <div class="col-md-1">
                            <?php echo $buyer_receipt["city"]; ?> 
                            </div>
                            <div class="col-md-1">
                            <?php echo $buyer_receipt["buyer_ip"]; ?> 
                            </div>
                            <div class="col-md-1">
                            <?php echo $buyer_receipt["receipt_id"]; ?> 
                            </div>
                            <div class="col-md-1">
                            <?php echo $buyer_receipt["items"]; ?>
                            </div>
                            <div class="col-md-1">
                            <?php echo $buyer_receipt["amount"]; ?>
                            </div>
                            <div class="col-md-1">
                            <?php echo $buyer_receipt["note"]; ?>
                            </div>
                            <div class="col-md-1">
                            <?php echo $buyer_receipt["entry_at"]; ?>
                            </div>
                            <div class="col-md-1">
                            <?php echo $buyer_receipt["entry_by"]; ?>
                            </div>
                            <div class="col-md-1">
                                <div >
                                    <a href="index.php?action=detail&id=<?php echo $buyer_receipt['id']; ?>" class="btn btn-info">Details</a>
                                </div>
                            </div>
                        </div> 
                    <?php } ?>
                 </div>
               </div>  
            </div>
        </div>
    </div> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    
   
<script type="text/javascript">
$(document).ready(function() {
    $(".call-btn").click(function(){
        var message="";
        $("#message").html(message);

     if($.trim($("#amount").val())=="")
       message +="amount could not be empty<br>";
     if($.trim($("#phone").val())=="")
        message +="phone could not be empty<br>";
     if($.trim($("#buyer_email").val())=="")
        message +="buyer_email could not be empty<br>";
     if($.trim($("#receipt_id").val())=="")
        message +="receipt_id could not be empty<br>";
     if($.trim($("#city").val())=="")
        message +="city could not be empty<br>";
     if($.trim($("#note").val())=="")
        message +="note could not be empty<br>";
     if($("#items :selected").length<1 )
        message +="items could not be empty<br>";
     if($.trim($("#buyer").val())=="")
        message +="buyer could not be empty<br>";
      //  else if($("#buyer").length>20){
      //  message +="buyer could character length can not be more than 20<br>";
       // }
    // return if not valid
    if(message!=""){
        $("#message").html(message);
            message="";
            return;
    }
    var formData = $("#rform").serializeArray();
    
        $.ajax({
            type: "POST",
            url: '/mvc/index.php?action=create',
            data: formData,
            dataType: "json",
            success: function(response)
            {
              if(response.flag==2){
                $("#message").html(response.message);
               }
              else if(response.flag==1 ){
                $(".inactive-btn").show();
                $(".call-btn").hide();
                $("#dynamic").prepend(response.div_content);
                $("#message").html(response.message);

               } else {
                $("#message").html(response.message);
               }
              
           }
       });
    });
   
});
</script>


</body>
</html>